<?php
/**
 * Die Datei enthält eine Klasse als Schnittstelle zu RKI_COVID19
 *
 * PHP Version 8
 *
 * @category  PHP
 * @package   Mibeg
 * @author    Michael Burgardt <mail@gmx.de>
 * @author    Marcel Königs <mail@gmx.de>
 * @author    Gaurav Dwivedi <mail@gmx.de>
 * @copyright 2021 mibeg
 * @license   BSD License
 * @link      https://clindat.mibeg-cms.de/
 */
/**
 * Die Klasse stellt eine Schnittstelle zu RKI_COVID19 dar
 *
 * PHP Version 8
 *
 * @category  PHP
 * @package   Mibeg
 * @author    Michael Burgardt <mail@gmx.de>
 * @author    Marcel Königs <mail@gmx.de>
 * @author    Gaurav Dwivedi <mail@gmx.de>
 * @copyright 2021 mibeg
 * @license   BSD License
 * @link      https://clindat.mibeg-cms.de/
 */

class RKIQuery
{
    /**
     * Landkreis, für den die Daten abgefragt werden
     *
     * @access private
     * @var    string
     */
    private $_stadt;
    
    /**
     * Assoziatives Array, wandelt Landkreisnamen zu ID um
     *
     * @access private
     * @var    array()
     */
    private $_stadt_id = ['SK Köln' => '05315', 'SK Düsseldorf' => '05111', 
                          'Städteregion Aachen' => '05334'];
    
    /**
     * Original vom RKI erhaltener JSON-Text
     *
     * @access private
     * @var    string
     */
    private $_json_text;
    
    /**
     * Array mit den relevanten Daten aus dem JSON der Form: 
     * array([Landkreis,Altersgruppe,Geschlecht,AnzahlFall,AnzahlGenesen,
     * AnzahlTodesfall,Meldedatum], ...)
     *
     * @access private
     * @var    array()
     */
    private $_array_form;
    
    /**
     * Diese Funktion kapselt $_stadt und ruft den entsprechenden Datensatz ab.
     *
     * @param string $stadt Landkreis
     *
     * @return void
     * @access public
     */
    public function setStadt($stadt)
    {
        if (array_key_exists($stadt, $this->_stadt_id)) {
            $this->_stadt = $stadt;
            $this->queryRKI();
        } else {
            throw new ErrorException(
                'Landkreis nicht bekannt!', 2001,
                E_WARNING, 'RKIQuery.php->setStadt()', 92
            );
        }
    }
    
    /**
     * Diese Funktion gibt die $_stadt zurück, für die der derzeitige Datensatz gilt.
     *
     * @return string
     * @access public
     */
    public function getStadt()
    {
        return $this->_stadt;
    }
    
    /**
     * Diese Funktion führt die Abrufanfrage beim RKI aus und
     * füllt $_json_text & $_array_form.
     *
     * @return void
     * @access public
     */
    public function queryRKI()
    {
        date_default_timezone_set('UTC');
        $date =  new DateTime();
        $date_today = strtotime($date->format('Y-m-d'));
        $date_yester = $date_today - 86400;
        $date_today = date('Ymd', $date_today);
        $date_yester = date('Ymd', $date_yester);
        $stadt_id = $this->_stadt_id[$this->_stadt];
        $url = "https://services7.arcgis.com/mOBPykOjAyBO2ZKk/arcgis/rest/" . 
        "services/RKI_COVID19/FeatureServer/0/query?where=IdLandkreis%20%3D%20" . 
        "'$stadt_id'%20AND%20%20(Meldedatum%20%3D%20'$date_yester'%20OR%20" . 
        "Meldedatum%20%3D%20'$date_today')%20&outFields=Landkreis,Altersgruppe," . 
        "Geschlecht,AnzahlFall,AnzahlGenesen,AnzahlTodesfall," .
        "Meldedatum&outSR=4326&f=json";
        
        $this->_json_text=file($url)[0];
        
        $json = json_decode($this->_json_text, true);
        $features = $json['features'];
        
        $result = array();
        foreach ($features as $key => $value) {
            $num_result = array_push($result, $value['attributes']);
        }
        $this->_array_form = $result;
    }
    
    /**
     * Diese Funktion ruft sämtliche Datensätze des RKI zu dem gekapselten
     * Landkreis ab und gibt diese in form eines Arrays wieder.
     *
     * @return void
     * @access public
     */
    public function getAllRKI()
    {
        $stadt_id = $this->_stadt_id[$this->_stadt];
        $url = "https://services7.arcgis.com/mOBPykOjAyBO2ZKk/arcgis/rest/" . 
            "services/RKI_COVID19/FeatureServer/0/query?where=IdLandkreis" .
            "%20%3D%20'$stadt_id'&outFields=Landkreis,Altersgruppe,Geschlecht," .
            "AnzahlFall,AnzahlGenesen,AnzahlTodesfall,Meldedatum&outSR=4326&f=json";
        
        $json_text=file($url)[0];
        $json = json_decode($json_text, true);
        $features = $json['features'];
        
        $result = array();
        foreach ($features as $key => $value) {
            $num_result = array_push($result, $value['attributes']);
        }
        return $result;
    }
    
    /**
     * Diese Funktion gibt den original erhaltenen JSON_Text zurück.
     *
     * @return string
     * @access public
     */
    public function getJsonText()
    {
        return $this->_json_text;
    }
    
    /**
     * Diese Funktion gibt den Array mit den relevanten Datensätzen zurück.
     *
     * @return array()
     * @access public
     */
    public function getArrayForm()
    {
        return $this->_array_form;
    }
}