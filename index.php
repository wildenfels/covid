<!doctype html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <link href="bootstrap-5.1.0-dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="bootstrap-5.1.0-dist/js/bootstrap.bundle.min.js"></script>
    <title>Covid19 Fälle</title>
</head>
<body>
    <noscript>Sie haben Javascript deaktiviert. Einige Sachen tun es nicht</noscript>
    <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="get">
    <button type="submit" class="btn btn-primary" style="margin-top: 25px;">Daten Aktualisieren</button>
    </form>
    <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="get">
        <select class="form-select" aria-hidden="true" style="width:250px;margin-top: 25px;" aria-label="Default select example" name="stadt" id="stadt">
            <option selected>Stadt wählen bitte!!</option>
            <option value="Aachen">Aachen</option>
            <option value="Düsseldorf">Düsseldorf</option>
            <option value="Köln ">Köln </option>
            <?php // fill_Stad($organ); ?>
        </select>
        <br />
        <br />
<!--
        <table class="table table-dark">
            <br><br>
            <tr>
                <th scope="col">Fälle gesamt</th>
                <th scope="col">Noch aktive Fälle</th>
                <th scope="col">Neue Fälle</th>
                <th scope="col">Fälle in den letzten 7 Tagen</th>
                <th scope="col">7-Tage-Inzidenz</th>
                <th scope="col">Todesfälle gesamt</th>
                <th scope="col">Neue Todesfälle</th>
                <th scope="col">Genesen gesamt</th>
                <th scope="col">Neu genesene</th>
            </tr>
            <tr class="table-primary">
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
                <td>5</td>
                <td>6</td>
                <td>7</td>

                <td>8</td>
                <td>3</td>
            </tr>
            -->

            <table class="table table-bordered table-dark" style="width: 75%;">
                <tr>
                    <th scope="col">Neue Covid19 Fälle</th>
                    <th scope="col">Neue Todesfälle durch Covid19</th>
                    <th scope="col">Von Covid19 Genesene</th>
                    <th scope="col">Seit gestern den</th>
                </tr>
                <tr class="table-danger">
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                    
                </tr>
            </table>
    </form>
</body>

</html>