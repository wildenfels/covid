<?php
/**
 * Datei enthält Klasse CoronaData zum Management von der zugehörigen Datenbank
 * und dient als Interface für Input und Output der Datenbank
 *
 * PHP version 8
 *
 * @category  File
 * @package   CoronaData
 * @author    Ruth Klopstein <ruth@example.com>
 * @author    Stephen Krapp <stephen@example.com>
 * @author    Damian Iwanicki <damian@example.com>
 * @copyright 2021 The Mibeg Group
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   GIT: 
 * @link      http://nolinkyet
 */

/**
 * Klasse zum Management von der zugehörigen Datenbank,
 * dient als Interface für Input und Output der Datenbank
 * 
 * @category  Class
 * @package   CoronaData
 * @author    Ruth Klopstein <ruth@example.com>
 * @author    Stephen Krapp <stephen@example.com>
 * @author    Damian Iwanicki <damian@example.com>
 * @copyright 2021 The Mibeg Group
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   Release:
 * @link      http://nolinkyet
 */
class CoronaData
{
    private $_dbCon;

    function __construct()
    {
        include_once 'DBConnection.php';
        $this->_dbCon = new DBConnection(); 
    }

    public function getAllDataAssoc()
    {
        $pdo = $this->_dbCon->get();

        $sql    = 
            "SELECT
                city.Stadt,
                agegroup.Altersgruppe,
                covid.Geschlecht,
                covid.FaelleNEU,
                covid.GenesenNEU,
                covid.ToteNEU,
                covid.Meldedatum,
                covid.Timestp
            FROM covid
            INNER JOIN city ON city.id = covid.Stadt
            INNER JOIN agegroup ON agegroup.id = covid.Altersgruppe
            ";
        $stmt = $pdo->query($sql);
        if(!$stmt)
        {
            echo "QUERY FAILED!";
        }

        return $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    // $tagesDatum in Form einer DateTime Klasse 
    public function getSumCityData_day(DateTime $tagesDatum)
    {
        $pdo = $this->_dbCon->get();

        // DateTime auf Tag kürzen und in Datenbankformat konvertieren
        $tagesDatum->setTimezone(new DateTimeZone("UTC"));
        $date_today = strtotime($tagesDatum->format('Y-m-d'));
        $date_today = ($date_today - 86400)*1000;

        $cities = $this->getCities();

        $result = array();
        foreach($cities as $city)
        {
            $sql    = 
            "SELECT
                SUM(covid.FaelleNEU) as AlleFaelle,
                SUM(covid.GenesenNEU) as AlleGenesen,
                SUM(covid.ToteNEU) as AlleToten
            FROM covid
            INNER JOIN city ON city.id = covid.Stadt
            INNER JOIN agegroup ON agegroup.id = covid.Altersgruppe
            WHERE covid.Meldedatum = $date_today
            AND city.Stadt LIKE '$city';";

            //echo $sql;

            $sth = $pdo->prepare($sql);
            $sth->execute();
            //print_r($sth->fetchAll(PDO::FETCH_ASSOC));

            $result[$city] = $sth->fetchAll(PDO::FETCH_ASSOC)[0];
        }
        //print_r($result);

        return $result;
    }

    public function getSumCityData_all()
    {
        $pdo = $this->_dbCon->get();

        // DateTime auf Tag kürzen und in Datenbankformat konvertieren

        $cities = $this->getCities();

        $result = array();
        foreach($cities as $city)
        {
            $sql    = 
            "SELECT
                SUM(covid.FaelleNEU) as AlleFaelle,
                SUM(covid.GenesenNEU) as AlleGenesen,
                SUM(covid.ToteNEU) as AlleToten
            FROM covid
            INNER JOIN city ON city.id = covid.Stadt
            INNER JOIN agegroup ON agegroup.id = covid.Altersgruppe
            WHERE city.Stadt LIKE '$city';";

            //echo $sql;

            $sth = $pdo->prepare($sql);
            $sth->execute();
            //print_r($sth->fetchAll(PDO::FETCH_ASSOC));

            $result[$city] = $sth->fetchAll(PDO::FETCH_ASSOC)[0];
        }
        //print_r($result);

        return $result;
    }

    public function querySQL($sql)
    {
        $pdo = $this->_dbCon->get();
        $sth = $pdo->prepare($sql);
        $sth->execute();
        // use $sth->fetchAll(OPTIONS) to get the data afterwards
        return $sth;
    }

    private function _writeEntryIntoDatabase(DatabaseEntry $databaseEntry)
    {
        include_once 'DatabaseEntry.php';

        $sql = 
        "INSERT IGNORE INTO covid 
            (id, Stadt, Altersgruppe, Geschlecht, FaelleNEU
            , GenesenNEU, ToteNEU, Meldedatum)
        VALUES (
            " . $databaseEntry->getId() . 
            ", " . $databaseEntry->getLandkreis() . 
            ", " . $databaseEntry->getAltersgruppe() . 
            ", " . $databaseEntry->getGeschlecht() .
            ", " . $databaseEntry->getAnzahlFall() .
            ", " . $databaseEntry->getAnzahlGenesen() .
            ", " . $databaseEntry->getAnzahlTodesfall() .
            ", " . $databaseEntry->getMeldedatum() .
            ")";

        //echo $sql;
        $pdo = $this->_dbCon->get();
        $sth = $pdo->prepare($sql);
        // returns true on success, false on failure
        return $sth->execute();
    }

    private function _convertRKIEntry_into_DbEntry($RKI_Entry)
    {
        /*
        $RKI_Entry Example ( 
            [Landkreis] => SK Köln 
            [Altersgruppe] => A05-A14 
            [Geschlecht] => M 
            [AnzahlFall] => 3 
            [AnzahlGenesen] => 0 
            [AnzahlTodesfall] => 0 
            [Meldedatum] => 1628380800000 );
        */

        //USING STATIC VALUES FROM DATABASE!!! COULD BE IMPROVED BY SQL QUERIES TO OBTAIN IDS
        $landkreis = 0;
        switch($RKI_Entry['Landkreis'])
        {
            case 'Städteregion Aachen':
                $landkreis = 1;
                break;
            case 'SK Köln':
                $landkreis = 2;
                break;
            case 'SK Düsseldorf':
                $landkreis = 3;
                break;
        }

        $altersgruppe = 0;
        switch($RKI_Entry['Altersgruppe'])
        {
            case 'A00-A04':
                $altersgruppe = 1;
                break;
            case 'A05-A14':
                $altersgruppe = 2;
                break;
            case 'A15-A34':
                $altersgruppe = 3;
                break;
            case 'A35-A59':
                $altersgruppe = 4;
                break;
            case 'A60-A79':
                $altersgruppe = 5;
                break;
            case 'A80+':
                $altersgruppe = 6;
                break;
            case 'unbekannt':
                $altersgruppe = 7;
                break;
        }

        // $geschlecht = 2 ist "unbekannt" und wird benutzt falls kein M oder W vorgefunden wird
        $geschlecht = 2;
        switch($RKI_Entry['Geschlecht'])
        {
            case 'M':
                $geschlecht = 0;
                break;
            case 'W':
                $geschlecht = 1;
                break;
        }

        // Eindeutiger Identifier (KEY) für die Datenbank einträge
        $id = $RKI_Entry['Meldedatum'] + $geschlecht + 10 * $altersgruppe + 100 * $landkreis;

        include_once 'DatabaseEntry.php';
        $dbentry = New DatabaseEntry($id
            , $landkreis
            , $altersgruppe
            , $geschlecht
            , $RKI_Entry['AnzahlFall']
            , $RKI_Entry['AnzahlGenesen']
            , $RKI_Entry['AnzahlTodesfall']
            , $RKI_Entry['Meldedatum']);
        return $dbentry;
    }

    public function getCities()
    {
        $pdo = $this->_dbCon->get();

        $sql    = 
            "SELECT
                Stadt
            FROM city
            ";
        $stmt = $pdo->query($sql);
        if(!$stmt)
        {
            echo "QUERY FAILED!";
        }
        
        $result = array();
        foreach ($stmt->fetchAll(PDO::FETCH_NUM) as $entry)
        {
            array_push($result, $entry[0]);
        }

        return $result;
    }

    public function writeDataIntoDatabase()
    {
        include_once 'DatabaseEntry.php';
        include_once 'RKIQuery.php';
        $cities = $this->getCities();
        $RKI_Data = new RKIQuery();
        foreach ($cities as $city)
        {
            $RKI_Data->setStadt($city);
            $converted_RKI_Data = $RKI_Data->getArrayForm();
            foreach ($converted_RKI_Data as $rki_entry)
            {
                $dbentry = $this->_convertRKIEntry_into_DbEntry($rki_entry);
                $this->_writeEntryIntoDatabase($dbentry);
            }
        }
        // loop over rki data and use _writeEntryIntoDatabase(DatabaseEntry $databaseEntry)
    }
}