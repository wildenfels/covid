<!doctype html>
<html lang="de">
<head>
  <title>Test</title>
  <meta charset="utf-8">
</head>
<body>
    <?php

        // Funktionalität um Daten von der RKI Schnittstelle auszulesen
        // und in unsere Datenbank einzutragen
        include_once 'src/CoronaData.php';
        $coronaData = new CoronaData();

/*
        include_once 'src/RKIQuery.php';
        $rkiQuery = New RKIQuery("SK Köln");
        $rkiData = $rkiQuery->getArrayForm();*/
        $coronaData->writeDataIntoDatabase(); //$rkiData);
/*
        $rkiQuery->setStadt("Städteregion Aachen");
        $rkiData = $rkiQuery->getArrayForm();
        $coronaData->writeDataIntoDatabase($rkiData);

        $rkiQuery->setStadt("SK Düsseldorf");
        $rkiData = $rkiQuery->getArrayForm();
        $coronaData->writeDataIntoDatabase($rkiData);
*/
        // Alle Daten aus der Datenbank auslesen in Form eines Arrays
        //$allData = $coronaData->getAllDataAssoc();
        //print_r($allData);

        // Alternativ ein SQL Query eingeben, Rückgabewert ist ein PDO_STATEMENT,
        // um die Daten zu erhalten.
        // Benutze $sth->fetchAll(OPTIONS) um die Daten zu erhalten
        /*
        $sql    = 
            "SELECT
                city.Stadt,
                agegroup.Altersgruppe,
                covid.Geschlecht,
                covid.FaelleNEU,
                covid.GenesenNEU,
                covid.ToteNEU,
                covid.Meldedatum,
                covid.Timestp
            FROM covid
            INNER JOIN city ON city.id = covid.Stadt
            INNER JOIN agegroup ON agegroup.id = covid.Altersgruppe
            ";
        */

        //$sth = $coronaData->querySQL($sql);
        //echo '<br><br>';
        //print_r($sth->fetchAll());

        // Städtedaten
        /*
            Array 
            ( 
                [Städteregion Aachen] => Array ( [AlleFaelle] => [AlleGenesen] => [AlleToten] => ) 
                [SK Köln] => Array ( [AlleFaelle] => 38 [AlleGenesen] => 0 [AlleToten] => 0 ) 
                [SK Düsseldorf] => Array ( [AlleFaelle] => 25 [AlleGenesen] => 0 [AlleToten] => 0 ) 
            )
        */

        //date_default_timezone_set('UTC');
        // LETZTER TAG 
        //$cityData_day = $coronaData->getSumCityData_day(new DateTime());
        //print_r($cityData_day);
        // ALLE DATEN
        //$cityData_all = $coronaData->getSumCityData_all();
        //print_r($cityData_all);

    ?>
</body>
</html>