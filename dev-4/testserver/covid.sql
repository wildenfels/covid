-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2021 at 12:55 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_iwanicki`
--

-- --------------------------------------------------------

--
-- Table structure for table `covid`
--

CREATE TABLE `covid` (
  `id` bigint(20) NOT NULL,
  `Stadt` int(11) NOT NULL,
  `Altersgruppe` int(11) NOT NULL,
  `Geschlecht` tinyint(4) DEFAULT NULL COMMENT '0=männlich, 1=weiblich, 2=unbekannt',
  `FaelleNEU` bigint(20) DEFAULT NULL,
  `GenesenNEU` bigint(20) DEFAULT NULL,
  `ToteNEU` bigint(20) DEFAULT NULL,
  `Meldedatum` bigint(20) DEFAULT NULL,
  `Timestp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `covid`
--

INSERT INTO `covid` (`id`, `Stadt`, `Altersgruppe`, `Geschlecht`, `FaelleNEU`, `GenesenNEU`, `ToteNEU`, `Meldedatum`, `Timestp`) VALUES
(115, 2, 2, 0, 1, 0, 0, 1628467200000, '2021-08-10 10:54:35'),
(116, 2, 2, 1, 4, 0, 0, 1628467200000, '2021-08-10 10:54:35'),
(117, 2, 3, 0, 9, 0, 0, 1628467200000, '2021-08-10 10:54:35'),
(118, 2, 3, 2, 2, 0, 0, 1628467200000, '2021-08-10 10:54:35'),
(119, 2, 3, 1, 10, 0, 0, 1628467200000, '2021-08-10 10:54:35'),
(120, 2, 4, 0, 5, 0, 0, 1628467200000, '2021-08-10 10:54:35'),
(121, 2, 4, 2, 1, 0, 0, 1628467200000, '2021-08-10 10:54:35'),
(122, 2, 4, 1, 4, 0, 0, 1628467200000, '2021-08-10 10:54:35'),
(123, 2, 5, 0, 1, 0, 0, 1628467200000, '2021-08-10 10:54:35'),
(124, 2, 5, 1, 1, 0, 0, 1628467200000, '2021-08-10 10:54:35'),
(125, 3, 2, 0, 1, 0, 0, 1628467200000, '2021-08-10 10:54:36'),
(126, 3, 2, 0, 2, 0, 0, 1628467200000, '2021-08-10 10:54:36'),
(127, 3, 1, 1, 1, 0, 0, 1628467200000, '2021-08-10 10:54:36'),
(128, 3, 3, 2, 1, 0, 0, 1628467200000, '2021-08-10 10:54:36'),
(129, 3, 3, 0, 1, 0, 0, 1628467200000, '2021-08-10 10:54:36'),
(130, 3, 3, 0, 1, 0, 0, 1628467200000, '2021-08-10 10:54:36'),
(131, 3, 3, 0, 1, 0, 0, 1628467200000, '2021-08-10 10:54:36'),
(132, 3, 3, 1, 1, 0, 0, 1628467200000, '2021-08-10 10:54:36'),
(133, 3, 3, 1, 2, 0, 0, 1628467200000, '2021-08-10 10:54:36'),
(134, 3, 4, 0, 2, 0, 0, 1628467200000, '2021-08-10 10:54:36'),
(135, 3, 4, 0, 1, 0, 0, 1628467200000, '2021-08-10 10:54:36'),
(136, 3, 4, 0, 1, 0, 0, 1628467200000, '2021-08-10 10:54:36'),
(137, 3, 4, 0, 1, 0, 0, 1628467200000, '2021-08-10 10:54:36'),
(138, 3, 4, 1, 1, 0, 0, 1628467200000, '2021-08-10 10:54:36'),
(139, 3, 4, 1, 2, 0, 0, 1628467200000, '2021-08-10 10:54:36'),
(140, 3, 4, 1, 3, 0, 0, 1628467200000, '2021-08-10 10:54:36'),
(141, 3, 6, 0, 1, 0, 0, 1628467200000, '2021-08-10 10:54:36'),
(142, 3, 5, 0, 1, 0, 0, 1628467200000, '2021-08-10 10:54:36'),
(143, 3, 5, 1, 1, 0, 0, 1628467200000, '2021-08-10 10:54:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `covid`
--
ALTER TABLE `covid`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Stadt` (`Stadt`),
  ADD KEY `Altersgruppe` (`Altersgruppe`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `covid`
--
ALTER TABLE `covid`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `covid`
--
ALTER TABLE `covid`
  ADD CONSTRAINT `covid_ibfk_1` FOREIGN KEY (`Stadt`) REFERENCES `city` (`id`),
  ADD CONSTRAINT `covid_ibfk_2` FOREIGN KEY (`Altersgruppe`) REFERENCES `agegroup` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
