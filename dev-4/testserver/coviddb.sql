-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 09. Aug 2021 um 15:09
-- Server-Version: 10.4.19-MariaDB
-- PHP-Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `coviddb`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `agegroup`
--

CREATE TABLE `agegroup` (
  `id` int(11) NOT NULL,
  `Altersgruppe` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `agegroup`
--

INSERT INTO `agegroup` (`id`, `Altersgruppe`) VALUES
(1, 'A00-A04'),
(2, 'A05-A14'),
(3, 'A15-A34'),
(4, 'A35-A59'),
(5, 'A60-A79'),
(6, 'A80+'),
(7, 'unbekannt');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `Stadt_id` varchar(250) DEFAULT NULL,
  `Stadt` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `city`
--

INSERT INTO `city` (`id`, `Stadt_id`, `Stadt`) VALUES
(1, '05334', 'Städteregion Aachen'),
(2, '05315', 'SK Köln'),
(3, '05111', 'SK Düsseldorf');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `covid`
--

CREATE TABLE `covid` (
  `id` bigint(20) NOT NULL,
  `Stadt` int(11) NOT NULL,
  `Altersgruppe` int(11) NOT NULL,
  `Geschlecht` tinyint(4) DEFAULT NULL COMMENT '0=männlich, 1=weiblich, 2=unbekannt',
  `Meldedatum` bigint(20) DEFAULT NULL,
  `FaelleNEU` bigint(20) DEFAULT NULL,
  `GenesenNEU` bigint(20) DEFAULT NULL,
  `ToteNEU` bigint(20) DEFAULT NULL,
  `Timestp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `covid`
--

INSERT INTO `covid` (`id`, `Stadt`, `Altersgruppe`, `Geschlecht`, `Meldedatum`, `FaelleNEU`, `GenesenNEU`, `ToteNEU`, `Timestp`) VALUES
(1, 1, 4, 2, 1586077500000, 5, 1, 1, '2021-08-09 13:08:23');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `agegroup`
--
ALTER TABLE `agegroup`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `covid`
--
ALTER TABLE `covid`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Stadt` (`Stadt`),
  ADD KEY `Altersgruppe` (`Altersgruppe`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `agegroup`
--
ALTER TABLE `agegroup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT für Tabelle `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `covid`
--
ALTER TABLE `covid`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `covid`
--
ALTER TABLE `covid`
  ADD CONSTRAINT `covid_ibfk_1` FOREIGN KEY (`Stadt`) REFERENCES `city` (`id`),
  ADD CONSTRAINT `covid_ibfk_2` FOREIGN KEY (`Altersgruppe`) REFERENCES `agegroup` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
