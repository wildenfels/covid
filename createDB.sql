DROP DATABASE covidDB;

CREATE DATABASE covidDB;

USE covidDB;

CREATE TABLE city (
    id integer NOT NULL AUTO_INCREMENT,
    Stadt_id varchar(250),
    Stadt varchar(250),
    PRIMARY KEY (id)
);

CREATE TABLE agegroup (
    id integer NOT NULL AUTO_INCREMENT,
    Altersgruppe varchar(250),
    PRIMARY KEY (id)
);

CREATE TABLE covid (
    id bigint NOT NULL,
    Stadt integer NOT NULL,
    Altersgruppe integer NOT NULL,
    Geschlecht tinyint,
    FaelleNEU bigint,
    GenesenNEU bigint,
    ToteNEU bigint,
    Meldedatum bigint,
    Timestp TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (Stadt) REFERENCES city (id),
    FOREIGN KEY (Altersgruppe) REFERENCES agegroup (id)
);

ALTER TABLE `covid` CHANGE `Geschlecht` `Geschlecht` TINYINT(4) NULL DEFAULT NULL COMMENT '0=männlich, 1=weiblich, 2=unbekannt'; 
    
INSERT INTO city (Stadt_id, Stadt) VALUES ('05334', 'Städteregion Aachen');
INSERT INTO city (Stadt_id,Stadt) VALUES ('05315', 'SK Köln');
INSERT INTO city (Stadt_id,Stadt) VALUES ('05111', 'SK Düsseldorf');

INSERT INTO agegroup (Altersgruppe) VALUES ('A00-A04');
INSERT INTO agegroup (Altersgruppe) VALUES ('A05-A14');
INSERT INTO agegroup (Altersgruppe) VALUES ('A15-A34');
INSERT INTO agegroup (Altersgruppe) VALUES ('A35-A59');
INSERT INTO agegroup (Altersgruppe) VALUES ('A60-A79');
INSERT INTO agegroup (Altersgruppe) VALUES ('A80+');
INSERT INTO agegroup (Altersgruppe) VALUES ('unbekannt');

/*INSERT INTO covid (Stadt, Altersgruppe, Geschlecht, FaelleNEU, GenesenNEU, ToteNEU, Meldedatum)
VALUES (
    (SELECT id FROM city WHERE Stadt_id = '05334'),
    (SELECT id FROM agegroup WHERE Altersgruppe = 'A35-A59'),
    2,
    5,
    1,
    1,
    1586077500000
);*/