# covid

Aufgabe:

Ermittlung und Darstellung im Browser der aktuellen COVID-19 Situation.

Für die Statistiken werden Zahlen des RKI (https://www.rki.de/DE/Home/homepage_node.html) verwendet. Das RKI aktualisiert seine Zahlen einmal täglich auf Grundlage der Meldungen der deutschen Gesundheitsämter. Die bereitgestellten Daten stehen unter der Open Data Datenlizenz Deutschland – Namensnennung – Version 2.0 (https://www.govdata.de/dl-de/by-2-0) zur Verfügung.

Beispiel: https://experience.arcgis.com/experience/478220a4c454480e823b17327b2bf1d4

Folgende Landkreise (im Folgenden genannt Städte) Aachen, Düsseldorf und Köln sind für uns relevant. Weitere Städte bitte mit dem gesamten Team abstimmen.

Folgende Felder sind für uns von Bedeutung: Erfassungsdatum, Fälle gesamt, noch aktive Fälle, neue Fälle, Fälle in den letzten 7 Tagen, 7-Tage-Inzidenz, Todesfälle gesamt, neue Todesfälle, genesen gesamt und neu genesene. Weitere Felder bitte mit dem gesamten Team abstimmen.

Erstelle Tabellen und Grafiken (Balken-Diagramme).

Folgende Felder sind in der Tabelle von Bedeutung: Stadt, Altersgruppe, Geschlecht, Anzahl Fälle, Anzahl Todesfälle und Anzahl genesen. Die ausgewerteten Daten sollten von allen Feldern aufsteigend oder absteigend sortierbar sein. Zusätzlich sollte es Filtermöglichkeiten geben von Stadt, Alter und Geschlecht.

In den Grafiken sollen die Daten für die Anzahl der Fälle, Anzahl der Todesfälle und die Anzahl der Genesenen gruppiert nach Altersgruppen(0-4, 5-14, 15-34, 35-59, 60-78, 80+, unbekannt) und Geschlecht gezeigt werden. Führen Sie die Daten der jeweils oben genannten Städte auf.
Speichern Sie die erfassten Daten in einer individuellen Datenbank.

Umsetzung:	 

dev-1: 

•	Entwicklung einer eigenen Datenbank.

•	Datenbank-Diagramm mit Beziehungen erstellen.

•	CREATE TABLE Statement erstellen.

•	Index(e) erstellen.

•	Gegebenenfalls Erstellung von CONSTRAINTS.

dev-2:

•	Schnittstelle zum RKI erstellen.

•	Die Daten werden über die Schnittstelle RKI_COVID19 über esri ArcGIS abgerufen (https://npgeo-corona-npgeo-de.hub.arcgis.com/datasets/
dd4580c810204019a7b8eb3e0b329dd6_0/api).

•	Dokumentation zur ArcGIS REST API (https://developers.arcgis.com/rest/services-reference/enterprise/query-feature-service-layer-.htm)

•	Abfrage-URL programmieren, um eine JSON-Antwort abzurufen.

dev-3:

•	Frontend erstellen mit HTML5 und CSS.

•	Einbinden des CSS Frameworks Bootstrap.

•	Einbinden des JavaScript Frameworks jQuery.

dev-4:

•	SQL programmieren in PHP (Mysqli oder PDO).

•	Prepared Statement oder mysqli_real_escape_string.

•	Abschließend CRON-Job für die tägliche Aktualisierung einrichten.
