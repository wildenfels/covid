<?php

class DBConnection
{
    private $_server = 'mysql:dbname=db_iwanicki;host=localhost';
    private $_dbuser = 'root';
    private $_dbpasswort = '';
    private $_options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');
    private $_pdo;

    function __construct()
    {
        $this->_pdo = new PDO($this->_server, $this->_dbuser, $this->_dbpasswort, $this->_options);
    }

    function get()
    {
        return $this->_pdo;
    }
}