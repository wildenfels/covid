<?php

class DatabaseEntry
{
    private $_id;
    private $_Landkreis;
    private $_Altersgruppe;	
    private $_Geschlecht;	
    private $_AnzahlFall;	
    private $_AnzahlTodesfall;	
    private $_AnzahlGenesen;
    private $_Meldedatum;

    function __construct(
        $id
        , $Landkreis
        , $Altersgruppe
        , $Geschlecht
        , $AnzahlFall
        , $AnzahlTodesfall
        , $AnzahlGenesen
        , $Meldedatum)
    {
        $this->_id = $id;
        $this->_Landkreis = $Landkreis;
        $this->_Altersgruppe = $Altersgruppe;
        $this->_Geschlecht = $Geschlecht;
        $this->_AnzahlFall = $AnzahlFall;
        $this->_AnzahlTodesfall = $AnzahlTodesfall;
        $this->_AnzahlGenesen = $AnzahlGenesen;
        $this->_Meldedatum = $Meldedatum;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getLandkreis()
    {
        return $this->_Landkreis;
    }

    public function getAltersgruppe()
    {
        return $this->_Altersgruppe;
    }

    public function getGeschlecht()
    {
        return $this->_Geschlecht;
    }

    public function getAnzahlFall()
    {
        return $this->_AnzahlFall;
    }

    public function getAnzahlTodesfall()
    {
        return $this->_AnzahlTodesfall;
    }

    public function getAnzahlGenesen()
    {
        return $this->_AnzahlGenesen;
    }

    public function getMeldedatum()
    {
        return $this->_Meldedatum;
    }

}